
### 22.2. Deleting the disklabel

Though this is not an operation that you need to perform frequently, it can be useful to know how to do it in case of need. Please be sure to know exactly what you are doing before performing this kind of operation. For example:

> dd if=/dev/zero of=/dev/rwd0c bs=8k count=1
The previous command deletes the disklabel (not the MBR partition table). To completely delete the disk, the whole device rwd0d must be used. For example:

> dd if=/dev/zero of=/dev/rwd0d bs=8k
The commands above will only work as expected on the i386 and amd64 ports of NetBSD. On other ports, the whole device will end in c, not d (e.g. rwd0c).
